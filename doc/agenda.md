1.	Introduction
2.  Questions for TA
    1. How should we distribute the work?
    2. What should be done by next week?
    3. What are some things to look out for when
        - tracing the rays?
        - building our scene?
    4. When should we start writing our report and working on the presentation?
    5. ...
2.	Agenda items
    1. Distribution of work
    2. Planning for coming week
4.	Any Other Business
5.	Action Points
    1.	Everyone should ....
6.	Next meeting – ...