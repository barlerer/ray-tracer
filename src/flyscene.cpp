#include "flyscene.hpp"
#include <GLFW/glfw3.h>

void Flyscene::initialize(int width, int height) {
	// initiliaze the Phong Shading effect for the Opengl Previewer
	phong.initialize();

	// set the camera's projection matrix
	flycamera.setPerspectiveMatrix(60.0, width / (float)height, 0.1f, 100.0f);
	flycamera.setViewport(Eigen::Vector2f((float)width, (float)height));

	// load the OBJ file and materials
	Tucano::MeshImporter::loadObjFile(mesh, materials, "resources/models/bunny.obj");
 

	// normalize the model (scale to unit cube and center at origin)
	mesh.normalizeModelMatrix();

	// pass all the materials to the Phong Shader
	for (int i = 0; i < materials.size(); ++i)
		phong.addMaterial(materials[i]);
	// set the color and size of the sphere to represent the light sources
	// same sphere is used for all sources
	lightrep.setColor(Eigen::Vector4f(1.0, 1.0, 0.0, 1.0));
	lightrep.setSize(0.15);

	// create a first ray-tracing light source at some random position
	lights.push_back(Eigen::Vector3f(-1.0, 2.0, 1.0));
    spheres.push_back(Sphere(Vector3f(3.0, 0.0, 0.0), 2));
    spheres.push_back(Sphere(Vector3f(0.0, 0.0, -3.0), 2));
    spheres.push_back(Sphere(Vector3f(-3.0, 0.0, 0.0), 2));

	// scale the camera representation (frustum) for the ray debug
	camerarep.shapeMatrix()->scale(0.2);

	// the debug ray is a cylinder, set the radius and length of the cylinder
	debugRay.setSize(0.005, 10.0);

	// craete a first debug ray pointing at the center of the screen
	createDebugRay(Eigen::Vector2f(width / 2.0, height / 2.0));

	glEnable(GL_DEPTH_TEST);

    vector<Tucano::Face> initialBoundingBox = Flyscene::InitializeBoundingBox();
    Flyscene::createBoundingBoxes(boundingStructure, initialBoundingBox , triangleParam);
    Flyscene::createBoundingBoxModels(boundingBoxModels, boundingStructure);

    std::cout << "mesh size: " << mesh.getNumberOfFaces();
    std::cout << " 1st box size: " << boundingStructure[0].size();
    std::cout << " 2nd box size: " << boundingStructure[1].size();
    std::cout << " box models size: " << boundingBoxModels.size();
}
void Flyscene::paintGL() {

	// update the camera view matrix with the last mouse interactions
	flycamera.updateViewMatrix();
	Eigen::Vector4f viewport = flycamera.getViewport();

	// clear the screen and set background color
	glClearColor(0.9, 0.9, 0.9, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// position the scene light at the last ray-tracing light source
	scene_light.resetViewMatrix();
	scene_light.viewMatrix()->translate(-lights.back());

	// render the scene using OpenGL and one light source
	phong.render(mesh, flycamera, scene_light);

	// render the ray and camera representation for ray debug
	debugRay.render(flycamera, scene_light);
	camerarep.render(flycamera, scene_light);

	// render ray tracing light sources as yellow spheres
	for (int i = 0; i < lights.size(); ++i) {
		lightrep.resetModelMatrix();
		lightrep.modelMatrix()->translate(lights[i]);
		lightrep.render(flycamera, scene_light);
	}
    for (int i = 0; i < spheres.size(); ++i) {
        lightrep.resetModelMatrix();
        lightrep.modelMatrix()->translate(spheres[i].center);
        lightrep.render(flycamera, scene_light);
    }
	// render coordinate system at lower right corner
	flycamera.renderAtCorner();

    //for (int i = 0; i < boundingBoxModels.size(); i++)
        //boundingBoxModels[i].render(flycamera, scene_light);
    
}
void Flyscene::simulate(GLFWwindow* window) {
	// Update the camera.
	// NOTE(mickvangelderen): GLFW 3.2 has a problem on ubuntu where some key
	// events are repeated: https://github.com/glfw/glfw/issues/747. Sucks.
	float dx = (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS ? 1.0 : 0.0) -
		(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS ? 1.0 : 0.0);
	float dy = (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS ||
		glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS
		? 1.0
		: 0.0) -
		(glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS ||
			glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS
			? 1.0
			: 0.0);
	float dz = (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS ? 1.0 : 0.0) -
		(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS ? 1.0 : 0.0);
	flycamera.translate(dx, dy, dz);
}
void Flyscene::createDebugRay(const Eigen::Vector2f& mouse_pos) {
	debugRay.resetModelMatrix();
	// from pixel position to world coordinates
	Eigen::Vector3f screen_pos = flycamera.screenToWorld(mouse_pos);

	// direction from camera center to click position
	Eigen::Vector3f dir = (screen_pos - flycamera.getCenter()).normalized();

	// position and orient the cylinder representing the ray
    Ray ray = Ray(flycamera.getCenter(), dir);
	debugRay.setOriginOrientation(flycamera.getCenter(), dir);

	// place the camera representation (frustum) on current camera location,
	camerarep.resetModelMatrix();
	camerarep.setModelMatrix(flycamera.getViewMatrix().inverse());

    Flyscene::debugBoundingBoxIntersection(ray);
    Flyscene::debugTriangleIntersection(ray);

}

void Flyscene::debugBoundingBoxIntersection(Ray &ray) {
   
    for (int i = 0; i < boundingBoxModels.size(); i++)
        boundingBoxModels[i].setColor(Eigen::Vector4f(1.0, 1.0, 0.1, 1.0));
   
    for (int i = 0; i < boundingBoxModels.size(); i++)
    {
        float tin = 0, tout = 0;
        Vector3f vmin = (boundingBoxModels[i].getShapeModelMatrix() * boundingBoxModels[i].vertices[0]).head<3>();
        Vector3f vmax = (boundingBoxModels[i].getShapeModelMatrix() * boundingBoxModels[i].vertices[6]).head<3>();

        if(Flyscene::intersectBox(ray, vmin, vmax, tin, tout))
            boundingBoxModels[i].setColor(Eigen::Vector4f(0.0, 0.0, 0.0, 1.0));
    }
    
}

void Flyscene::debugTriangleIntersection(Ray& ray) {

 
    for (int i = 0; i < mesh.getNumberOfFaces(); i++)
    {
        float dist = -1;
        Tucano::Face face = mesh.getFace(i);
        Vector3f v0 = (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[0])).head<3>();
        Vector3f v1 = (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[1])).head<3>();
        Vector3f v2 = (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[2])).head<3>();

        if (Flyscene::intersectTriangle(ray, v0, v1, v2, face.normal.normalized(), dist))
            std::cout << "hit face " << i << endl;
               
    }

}
void Flyscene::printMeshInfo()
{
	for (int i = 0; i < mesh.getNumberOfFaces(); ++i)
	{
		Tucano::Face face = mesh.getFace(i);
		std::cout << "face " << i << std::endl;
		for (int j = 0; j < face.vertex_ids.size(); ++j)
		{
			std::cout << "vid " << j << " " << face.vertex_ids[j]
				<< std::endl;
			std::cout << "vertex " <<
				mesh.getVertex(face.vertex_ids[j]).transpose()
				<< std::endl;
			std::cout << "normal " <<
				mesh.getNormal(face.vertex_ids[j]).transpose()
				<< std::endl;
		}
		std::cout << "mat id " << face.material_id << std::endl << std::endl;
		std::cout << "face normal " << face.normal.transpose() << std::endl;
	}
}
void Flyscene::raytracePart(int modIndex, int n, int width, int height, vector<vector<Eigen::Vector3f>>& pixel_data)
{
	// origin of the ray is always the camera center
	Eigen::Vector3f origin = flycamera.getCenter();
	Eigen::Vector3f screen_coords;
	// for every pixel shoot a ray from the origin through the pixel coords
	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < width; ++j) {
            if ((i * width + j) % n == modIndex)
            {
                // create a ray from the camera passing through the pixel (i,j)
                screen_coords = flycamera.screenToWorld(Eigen::Vector2f(j,i));
                Ray r = Ray(origin, (screen_coords - origin).normalized());
                // launch raytracing for the given ray and write result to pixel data
                pixel_data[i][j] = traceRay(r, 0);
            }
		}
	}
}
void Flyscene::raytraceScene(int width, int height) {
	std::cout << "ray tracing ..." << std::endl;

	// if no width or height passed, use dimensions of current viewport
	Eigen::Vector2i image_size(width, height);
	if (width == 0 || height == 0) {
		image_size = flycamera.getViewportSize();
	}
	if (OUTPUT_DEBUG)
	{
		printMeshInfo();
	}
	// create 2d vector to hold pixel colors and resize to match image size
	vector<vector<Eigen::Vector3f>> pixel_data;
	pixel_data.resize(image_size[1]);
	for (int i = 0; i < image_size[1]; ++i)
		pixel_data[i].resize(image_size[0]);

	if (MULTITHREADING)
	{
		int threadCount = thread::hardware_concurrency();
		vector<thread> threads;
		for (int i = 0; i < threadCount; i++)
		{
			threads.push_back(thread(&Flyscene::raytracePart, this, i, threadCount, image_size[0], image_size[1], std::ref(pixel_data)));
		}
		for (int i = 0; i < threadCount; i++)
		{
			threads[i].join();
		}
	}
	else
	{
		raytracePart(0,1,image_size[0], image_size[1], pixel_data);
	}

	std::cout << "ray tracing completed, writing to file..." << std::endl;
	// write the ray tracing result to a PPM image
	Tucano::ImageImporter::writePPMImage("result.ppm", pixel_data);
	std::cout << "completed writing to file" << std::endl;
}
Eigen::Vector3f Flyscene::traceRay(Ray &ray, int level) {
	Surface hit;
	if (intersect(ray, hit, level))
	{
        return shade(ray, hit, level);
        //return WHITE;
	}
	else return WHITE;
}


std::tuple<Vector3f, Vector3f> computeReflectedRay(Surface &hit) {
    // TODO:
    // * Check if surface is glossy/specular
    // * calculate new vector to be traced

    return std::tuple<Vector3f, Vector3f>();
}

Eigen::Vector3f Flyscene::shade(Ray &ray, Surface& hit, int level) {
    //place holder
    int mat_id = 0;

    Vector3f kd = Vector3f(0.0, 1.0, 0.0);//materials[mat_id].getDiffuse();
    Vector3f ka = Vector3f(1.0, 0.0, 0.0);//materials[mat_id].getAmbient();
    Vector3f ks = Vector3f(0.0, 0.0, 1.0);//materials[mat_id].getSpecular();

    Vector3f normal = (hit.normal).normalized();
    Vector3f position = hit.hitPos;
    Vector3f lightDir = -(hit.hitPos-scene_light.getCenter()).normalized();
    Vector3f viewDir = (position - ray.origin).normalized();
    float lightDirDotNormal = lightDir.dot(normal);
    Vector3f lightReflection = (lightDir - 2 * normal * lightDirDotNormal).normalized();
    ray.dir = (ray.dir - 2 * normal * ray.dir.dot(normal)).normalized();
    ray.origin = hit.hitPos+ray.dir*0.1f;
    float angle = max(lightDirDotNormal,0.0f);

    Vector3f light_intensity = lightrep.getColor().head<3>();

    return light_intensity.cwiseProduct(ka
        + kd * angle
        + ks * powf(max(lightReflection.dot(viewDir), 0.0f), 32)) + 0.1 * traceRay(ray, level + 1);
}
bool Flyscene::intersect(Ray &ray, Surface& hit, int level)
{

    if (level >= MAX_LEVEL) {
        return false;
    }
    float minDist = -1;
    for (int i = 0; i < boundingBoxModels.size(); i++)
    {
        float tin = 0, tout = 0;
        Vector3f vmin = (boundingBoxModels[i].getShapeModelMatrix() * boundingBoxModels[i].vertices[0]).head<3>();
        Vector3f vmax = (boundingBoxModels[i].getShapeModelMatrix() * boundingBoxModels[i].vertices[6]).head<3>();

        if (Flyscene::intersectBox(ray, vmin, vmax, tin, tout)) 
        {

            for (int j = 0; j < boundingStructure[i].size(); ++j)
            {
                Tucano::Face face = boundingStructure[i][j];
                float dist = -1;
                if (intersectTriangle(ray, (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[0])).head<3>(),
                    (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[1])).head<3>(), (mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[2])).head<3>(),
                    face.normal.normalized(), dist))
                {
                    if (minDist == -1 || dist < minDist) {
                        minDist = dist;
                        hit = Surface(ray.origin + dist * ray.dir, face.normal);
                    }
                }
            }
        }
    }
    for (int i = 0; i < spheres.size(); i++)
    {
        float dist = -1;
        if (intersectSphere(ray, spheres[i], dist))
        {
            if (minDist == -1 || dist < minDist) {
                minDist = dist;
                Vector3f pointAlongRay = ray.pointAlongRay(dist);
                hit = Surface(pointAlongRay, (pointAlongRay - spheres[i].center).normalized());
            }
        }
    }
	return minDist!=-1;
}
bool Flyscene::intersectDisk(Ray &ray, Vector3f origin, Vector3f n, float radius)
{
	float dist = 0;
	if (intersectPlane(ray, origin, n, dist)) {
		Vector3f p = ray.pointAlongRay(dist);
		Vector3f v = p - origin;
		float d2 = v.dot(v);
		//Optimized form with both sides squared
		return d2 <= radius * radius;
	}
	return false;
}
bool Flyscene::intersectPlane(Ray &ray, Vector3f& origin, Vector3f n, float& dist)
{
	float denom = n.dot(ray.dir);
	if (denom < -1e-6) {
		dist = (origin - ray.origin).dot(n) / denom;
		return (dist >= 0);
	}
	return false;
}
bool Flyscene::intersectTriangle(Ray &ray,
	Vector3f v0, Vector3f v1, Vector3f v2, Vector3f n, float& dist) {
	//This is either false or it gives us true + the distance in dir vectors from origin to the plane
	if (intersectPlane(ray, v0, n, dist))
	{
		//intersection with triangle plane using the t from the plane intersection test
		Vector3f P = ray.pointAlongRay(dist);
		//compute if p is inside the triangle
        
        Vector3f C;

        Vector3f edge0 = v1 - v0;
        Vector3f vp0 = P - v0;
        C = edge0.cross(vp0);
        if (n.dot(C) < 0) return false; // P is on the right side 

        // edge 1
        Vector3f edge1 = v2 - v1;
        Vector3f vp1 = P - v1;
        C = edge1.cross(vp1);
        if (n.dot(C) < 0)  return false; // P is on the right side 

        // edge 2
        Vector3f edge2 = v0 - v2;
        Vector3f vp2 = P - v2;
        C = edge2.cross(vp2);
        if (n.dot(C) < 0) return false; // P is on the right side; 

        return true;
	}
	return false;
}
bool Flyscene::intersectBox(Ray &ray,
	Vector3f vmin, Vector3f vmax, float& tin, float& tout) {

	float txmin = (vmin.x() - ray.origin.x()) / ray.dir.x();
	float txmax = (vmax.x() - ray.origin.x()) / ray.dir.x();
	float txin = min(txmin, txmax);
	float txout = max(txmin, txmax);

	float tymin = (vmin.y() - ray.origin.y()) / ray.dir.y();
	float tymax = (vmax.y() - ray.origin.y()) / ray.dir.y();
	float tyin = min(tymin, tymax);
	float tyout = max(tymin, tymax);

	float tzmin = (vmin.z() - ray.origin.z()) / ray.dir.z();
	float tzmax = (vmax.z() - ray.origin.z()) / ray.dir.z();
	float tzin = min(tzmin, tzmax);
	float tzout = max(tzmin, tzmax);

	tin = max(max(txin, tyin), tzin);
	tout = min(min(txout, tyout), tzout);
	if ((tin > tout) || (tout < 0)) {
		return false;
	}
	return true;
}
bool Flyscene::intersectSphere(Ray &ray, Sphere& sphere, float& t) {
    float t0, t1;
    Vector3f oc = ray.origin - sphere.center;
    float a = ray.dir.dot(ray.dir);
    float b = 2.0 * oc.dot(ray.dir);
    float c = oc.dot(oc) - sphere.radius * sphere.radius;
    float discriminant = b * b - 4 * a * c;
    if (discriminant < 0.0) 
    {
        return false;
    }
    else 
    {
        t0 = (-b - sqrt(discriminant)) / (2.0 * a);
        t1 = (-b + sqrt(discriminant)) / (2.0 * a);

        if (t0 > 0.0) {
            t = t0;
            return true;
        }
        if (t1 > 0.0) {
            t = t1;
            return true;
        }
        else {
            return false;
        }
    }
}

void Flyscene::acceleration() {


}

void Flyscene::createBoundingBoxModels(vector<Tucano::Shapes::Box> &boundingBoxModels,
    vector<vector<Tucano::Face>> boundingStructure) {

    for (int i = 0; i < boundingStructure.size(); i++)
        boundingBoxModels.push_back(createSingleBoxModel(boundingStructure[i])); 
}

Tucano::Shapes::Box Flyscene::createSingleBoxModel(vector<Tucano::Face> boundingBox) {

    float xmin = INT_MAX;
    float xmax = INT_MIN;
    float ymin = INT_MAX;
    float ymax = INT_MIN;
    float zmin = INT_MAX;
    float zmax = INT_MIN;
    for (int i = 0; i < boundingBox.size(); i++)
    {
        Tucano::Face face = boundingBox[i];
        for (int j = 0; j < face.vertex_ids.size(); j++)
        {
            Eigen::Vector4f vertex = mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[j]);
            if (vertex.x() > xmax)
                xmax = vertex.x();
            if (vertex.x() < xmin)
                xmin = vertex.x();
            if (vertex.y() > ymax)
                ymax = vertex.y();
            if (vertex.y() < ymin)
                ymin = vertex.y();
            if (vertex.z() > zmax)
                zmax = vertex.z();
            if (vertex.z() < zmin)
                zmin = vertex.z();
        }   
    }

    float width = xmax - xmin;
    float height = ymax - ymin;
    float depth = zmax - zmin;

    Tucano::Shapes::Box box = Tucano::Shapes::Box(width, height, depth);
    //box.setColor(Eigen::Vector4f(rand() / (float)RAND_MAX, rand() / (float)RAND_MAX, rand() / (float)RAND_MAX, 1.0));
    box.setColor(Eigen::Vector4f(1.0 , 1.0, 0.1, 1.0));

    // the center of the box needs to be equal to the center of the triangles object
    box.modelMatrix()->translate(Vector3f((xmax + xmin) / 2, (ymax + ymin) / 2, (zmax + zmin) / 2));

    return box;
}


vector<Tucano::Face> Flyscene::InitializeBoundingBox() {

    vector<Tucano::Face> boundingBox;
    for (int i = 0; i < mesh.getNumberOfFaces(); i++)
        boundingBox.push_back(mesh.getFace(i));
    
    return boundingBox;
}

void Flyscene::createBoundingBoxes(vector<vector<Tucano::Face>> &boundingStructure, vector<Tucano::Face> boundingBox, const int triangleParam){

    if (boundingBox.size() >= triangleParam)
    {
        vector<Tucano::Face> splitBoxLeft;
        vector<Tucano::Face> splitBoxRight;
        splitBox(boundingBox, splitBoxLeft, splitBoxRight);
        createBoundingBoxes(boundingStructure, splitBoxLeft, triangleParam);
        createBoundingBoxes(boundingStructure, splitBoxRight, triangleParam);
    }
    else
        boundingStructure.push_back(boundingBox); 
}

void Flyscene::splitBox(vector<Tucano::Face> boundingBox, vector<Tucano::Face> &splitBoxLeft, vector<Tucano::Face> &splitBoxRight) {

    float xmin = INT_MAX;
    float xmax = INT_MIN;
    float ymin = INT_MAX;
    float ymax = INT_MIN;
    float zmin = INT_MAX;
    float zmax = INT_MIN;
    float xAvg = 0, yAvg = 0, zAvg = 0;

    Flyscene::calculateBoxBoundaries(xmin, xmax, ymin, ymax, zmin, zmax, xAvg, yAvg, zAvg, boundingBox);

    xAvg /= 3 * boundingBox.size();
    yAvg /= 3 * boundingBox.size();
    zAvg /= 3 * boundingBox.size();

    float longestAxis = std::max(std::max((xmax - xmin), (ymax - ymin)), (zmax - zmin));
    char splitAxis = 'x';
    if (longestAxis == xmax - xmin)
        splitAxis = 'x';
    else if (longestAxis == ymax - ymin)
        splitAxis = 'y';
    else if (longestAxis == zmax - zmin)
        splitAxis = 'z';


    for (int i = 0; i < boundingBox.size(); i++)
    {
        Tucano::Face face = boundingBox[i];
        int axisCoordIsLeft = 0;
        int axisCoordIsRight = 0;
        for (int j = 0; j < face.vertex_ids.size(); j++)
        {
            Eigen::Vector4f vertex = mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[j]);
            if (splitAxis == 'x')
            {
                if (vertex.x() <= xAvg)
                    axisCoordIsLeft++;
                else
                    axisCoordIsRight++;
            }
            else if (splitAxis == 'y')
            {
                if (vertex.y() <= yAvg)
                    axisCoordIsLeft++;
                else
                    axisCoordIsRight++;
            }
            else if (splitAxis == 'z')
            {
                if (vertex.z() <= zAvg)
                    axisCoordIsLeft++;
                else
                    axisCoordIsRight++;
            }

        }

        if (axisCoordIsRight == 0)
            splitBoxLeft.push_back(face);
        else if (axisCoordIsLeft == 0)
            splitBoxRight.push_back(face);
        else
        {
            splitBoxLeft.push_back(face);
            splitBoxRight.push_back(face);
        }
    }
}

void Flyscene::calculateBoxBoundaries(float &xmin, float& xmax, float& ymin, float& ymax, float& zmin, float& zmax, 
    float &xAvg, float &yAvg, float &zAvg, vector<Tucano::Face> boundingBox) {

    for (int i = 0; i < boundingBox.size(); i++)
    {

        Tucano::Face face = boundingBox[i];
        for (int j = 0; j < face.vertex_ids.size(); j++)
        {

            Eigen::Vector4f vertex = mesh.getShapeModelMatrix() * mesh.getVertex(face.vertex_ids[j]);
            if (vertex.x() > xmax)
                xmax = vertex.x();
            if (vertex.x() < xmin)
                xmin = vertex.x();
            if (vertex.y() > ymax)
                ymax = vertex.y();
            if (vertex.y() < ymin)
                ymin = vertex.y();
            if (vertex.z() > zmax)
                zmax = vertex.z();
            if (vertex.z() < zmin)
                zmin = vertex.z();

            xAvg += vertex.x();
            yAvg += vertex.y();
            zAvg += vertex.z();
        }
    }
}

