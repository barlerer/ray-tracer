#ifndef __FLYSCENE__
#define __FLYSCENE__

#define BLACK Eigen::Vector3f(0.0,0.0,0.0)
#define WHITE Eigen::Vector3f(1.0,1.0,1.0)
#define MULTITHREADING true
#define OUTPUT_DEBUG false
#define MAX_LEVEL 4
// Must be included before glfw.
#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <tucano/effects/phongmaterialshader.hpp>
#include <tucano/mesh.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/shapes/cylinder.hpp>
#include <tucano/shapes/sphere.hpp>
#include <tucano/shapes/box.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/imageIO.hpp>
#include <tucano/utils/mtlIO.hpp>
#include <tucano/utils/objimporter.hpp>

#include "surface.h"
#include "ray.h"
#include "sphere.h"
#include <limits>
#include <thread>

using namespace Eigen;
class Flyscene {

public:
  Flyscene(void) {}

  /**
   * @brief Initializes the shader effect
   * @param width Window width in pixels
   * @param height Window height in pixels
   */
  void initialize(int width, int height);

  /**
   * Repaints screen buffer.
   **/
  virtual void paintGL();

  /**
   * Perform a single simulation step.
   **/
  virtual void simulate(GLFWwindow *window);

  /**
   * Returns the pointer to the flycamera instance
   * @return pointer to flycamera
   **/
  Tucano::Flycamera *getCamera(void) { return &flycamera; }

  /**
   * @brief Add a new light source
   */
  void addLight(void) { lights.push_back(flycamera.getCenter()); }

  /**
   * @brief Create a debug ray at the current camera location and passing
   * through pixel that mouse is over
   * @param mouse_pos Mouse cursor position in pixels
   */
  void createDebugRay(const Eigen::Vector2f &mouse_pos);
  void debugBoundingBoxIntersection(Ray &ray);
  void debugTriangleIntersection(Ray& ray);
  void printMeshInfo();

  /**
   * @brief raytrace your scene from current camera position   
   */
  void raytraceScene(int width = 0, int height = 0);
  void raytracePart(int modIndex, int n, int width, int height, vector<vector<Eigen::Vector3f>> &pixel_data);

  /**
   * @brief trace a single ray from the camera passing through dest
   * @param origin Ray origin
   * @param dest Other point on the ray, usually screen coordinates
   * @return a RGB color
   */
  Eigen::Vector3f traceRay(Ray &ray, int level);
  Eigen::Vector3f shade(Ray &ray, Surface& hit, int level);


  /**
   * Computes the reflected ray.
   * @param hit the hit surface.
   * @return a tuple containing a (origin,dest) to be used for tracing.
   */
  std::tuple<Vector3f, Vector3f> computeReflectedRay(Surface &hit);

    /**
 * @brief Checks a ray for intersection with the mesh
 * @param origin Ray origin
 * @param dest Other point on the ray, usually screen coordinates
 * @Param normalColor temporary color reference used to determine pixel color
 * @return whether the ray intersected and for now the normal of the surface it intersected with
 */
  bool intersect(Ray &ray, Surface& hit, int level);
  /**
 * @brief Checks a ray for intersection with a given triangle
 * @param origin Ray origin
 * @param dir Direction of the ray
 * @param v0 the zeroth triangle vertex
 * @param v1 the first triangle vertex
 * @param v2 the second triangle vertex
 * @param n the precalculated surface normal
 * @param t a depth variable reference
 * @return true if there was an intersection with the triangle
 */
  bool intersectTriangle(Ray &ray, Vector3f v0, Vector3f v1, Vector3f v2, Vector3f n, float& t);
  /**
* @brief Checks a ray for intersection with a given plane
* @param origin plane origin or offset
* @param n the precalculated surface normal
* @param rayOrigin Ray origin
* @param rayDir Direction of the ray
* @param t a depth variable reference
* @return true if there was an intersection with the plane
*/
  bool intersectPlane(Ray& ray, Vector3f& origin, Vector3f n, float& t);
  /**
* @brief Checks a ray for intersection with a given plane
* @param origin disk origin or offset
* @param n the precalculated surface norma
* @param radius radius of the disk
* @param rayOrigin Ray origin
* @param rayDir Direction of the ray
* @return true if there was an intersection with the disk
*/
  bool intersectDisk(Ray& ray, Vector3f origin, Vector3f n, float radius);
  bool intersectBox(Ray& ray, Vector3f vmin, Vector3f vmax, float& tin, float &tout);
  bool intersectSphere(Ray& ray, Sphere &sphere, float& t);


  void acceleration();

  Tucano::Shapes::Box createSingleBoxModel(vector<Tucano::Face> boundingBox);

  void createBoundingBoxModels(vector<Tucano::Shapes::Box> &boundingBoxModels, 
      vector<vector<Tucano::Face>> boundingStructure);

  vector<Tucano::Face> InitializeBoundingBox();

  void createBoundingBoxes(vector<vector<Tucano::Face>> &boundingStructure, vector<Tucano::Face> boundingBox, const int triangleParam);

  void splitBox(vector<Tucano::Face> boundingBox, vector<Tucano::Face>& splitBoxLeft, vector<Tucano::Face>& splitBoxRight);

  void calculateBoxBoundaries(float& xmin, float& xmax, float& ymin, float& ymax, float& zmin, float& zmax,
      float& xAvg, float& yAvg, float& zAvg, vector<Tucano::Face> boundingBox);


private:
  // A simple phong shader for rendering meshes
  Tucano::Effects::PhongMaterial phong;

  // A fly through camera
  Tucano::Flycamera flycamera;

  // the size of the image generated by ray tracing
  Eigen::Vector2i raytracing_image_size;

  // A camera representation for animating path (false means that we do not
  // render front face)
  Tucano::Shapes::CameraRep camerarep = Tucano::Shapes::CameraRep(false);

  // a frustum to represent the camera in the scene
  Tucano::Shapes::Sphere lightrep;

  // light sources for ray tracing
  vector<Eigen::Vector3f> lights;
  vector<Sphere> spheres;

  // Scene light represented as a camera
  Tucano::Camera scene_light;

  /// A very thin cylinder to draw a debug ray
  Tucano::Shapes::Cylinder debugRay = Tucano::Shapes::Cylinder(0.1, 1.0, 16, 64);

  // Scene meshes
  Tucano::Mesh mesh;
  Tucano::Mesh traceQuad;

  /// MTL materials
  vector<Tucano::Material::Mtl> materials;

  Sphere sphere;
  // Bounding Boxes Scene Models
  vector<Tucano::Shapes::Box> boundingBoxModels;

  // Bounded Boxes
  vector<vector<Tucano::Face>> boundingStructure;

  // Triangle param
  int triangleParam = 1000;

 
};

#endif // FLYSCENE
