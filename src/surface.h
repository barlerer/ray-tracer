#ifndef __SURFACE__
#define __SURFACE__
#include <tucano/mesh.hpp>

using namespace Eigen;
struct Surface
{
	Vector3f hitPos;
	Vector3f normal;
	Surface();
	Surface(Vector3f hitPos, Vector3f normal);
};
#endif
