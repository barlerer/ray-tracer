#ifndef __RAY__
#define __RAY__
#include <tucano/mesh.hpp>

using namespace Eigen;
struct Ray
{
	Vector3f origin;
	Vector3f dir;
    Vector3f pointAlongRay(float distance);
	Ray();
	Ray(Vector3f origin, Vector3f dir);
};
#endif
