#ifndef __MYSPHERE__
#define __MYSPHERE__
#include <tucano/mesh.hpp>

using namespace Eigen;
struct Sphere
{
	Vector3f center;
	float radius;
	Sphere();
	Sphere(Vector3f center, float radius);
};
#endif
